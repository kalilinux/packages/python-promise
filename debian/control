Source: python-promise
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3-all,
               python3-setuptools,
# for the tests
               python3-pytest,
	       python3-mock,
	       python3-pytest-benchmark
Standards-Version: 4.3.0
Homepage: https://github.com/syrusakbary/promise
Vcs-Git: https://gitlab.com/kalilinux/packages/python-promise.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/python-promise
Testsuite: autopkgtest-pkg-python

Package: python3-promise
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: Performant Promise implementation in Python 3
 This package contains an implementation of Promises in Python. It is a super
 set of Promises/A+ designed to have readable, performant code and to provide
 just the extensions that are absolutely necessary for using promises in Python.
 .
 Its fully compatible with the Promises/A+ spec.
 .
 This package installs the library for Python 3.
